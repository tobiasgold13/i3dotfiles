# i3dotfiles

I'm currently fooling around with i3 on an old Lenovo T430. I know I will break the config at some point and won't recognize it until it's too late, so I decided to add it to version control.
As I like checking other people's configs, I might as well share mine here.
Any improvements are welcome.

> The structure of this repo starts in my home dir (~/). Most config files are in .config folder.

## Dependencies
A (most likely not) complete list of packages and tools I use for my i3 desktop. I use Arch Linux, so these will be Arch Linux packages from the official repos or the aur.

- i3-gaps
- i3-status
- i3lock-color-git [aur]
- unclutter-xfixes-git 
    - for auto hiding the mouse cursor, as I don't need it many times
    - config in .xinitrc
- feh
    - obviously for desktop background drawing
    - started in .xinitrc
- termite
    - terminal emulator
- compton
    - for transparency e.g. in termite
- networkmanager
    - w/ network-manager-applet
    - just install the packages, enable the NetworkManager.service and start the applet with i3 (see i3 config)

### Lock-Screen

![i3lock-fancy screenshot](screenshots/i3lock_fancy_20180601.png)

I wanted a nice lockscreen, so I decided to use [i3lock-fancy](https://github.com/meskarune/i3lock-fancy). You can install [i3lock-fancy-git](https://aur.archlinux.org/packages/i3lock-fancy-git/) from aur and use it with *i3lock-fancy* cmd. See i3 config file for my start command.

### Bluetooth
I wanted to stream music via bluetooth. I installed pulseaudio incl. dependencies for bluetooth streaming and blueberry for a graphical bluetooth connecting ui (pretty buggy but works for connecting). I'm not sure which packages have to be explicitly installed, but these should be the relevant ones currently on my system, everything from the official arch linux repos:

* pulseaudio
* pavucontrol
* pulseaudio-alsa
* pulseaudio-bluetooth
* bluez
* blueberry

## ZSH

Install & configure zsh manually.

### packages

* zsh
* oh-my-zsh-git (aur)
* nerd-fonts-hack (aur)
* ttf-nerd-fonts-symbols (aur)
* autojump (for *autojump* plugin)
* xclip (for *copydir* plugin)
  
### configuration

As I use termite, copy desired theme from [here](https://github.com/khamer/base16-termite) as the termite config (or append). Adjust font (see in the corresponding termite config in this repo).

```shell
cp /usr/share/oh-my-zsh/zshrc ~/.zshrc
```

Adjust theme, if other than default is desired.

#### syntax highlighting and autocomplete

Install the following packages from the official repos and manually source the plugin files in *.zshrc*, because somehow this does not work with *oh-my-zsh*... (I guess it's just a small configuration to make it work, but I don't care).

* zsh-syntax-highlighting
* zsh-autosuggestions

## disable optical drive eject button

1. See [this](https://askubuntu.com/questions/571068/how-do-i-disable-laptop-optical-drive-eject-button-and-assign-eject-to-a-keyboar) thread and use udev rules method to enable blocking eject button.
2. Block eject button during start of i3 session (see i3 config in this repo).

## optimized reverse search
Install *fzf* package from official repos for an even better reverse search experience. Just source the corresponding files for bash or zsh. See configuration [here](https://wiki.archlinux.org/index.php/Fzf).