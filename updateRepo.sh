#!/bin/bash

# This script just copies a new version from my home directory to the repo.
# It's faster to update the repo with the actual files I use and I won't forget any.

# remove all files in repo root
find . -maxdepth 1 ! -name README.md ! -name updateRepo.sh ! -name .gitlab-ci.yml -type f -delete
# remove all dirs except .git
find . -maxdepth 1 ! -name .git ! -name screenshots ! -name . ! -name .. -type d -exec rm -r {} +

# create needed dirs
mkdir ./.config/

# copy relevant files
cp ~/.xinitrc ./.xinitrc
cp ~/.Xdefaults ./.Xdefaults
cp -r ~/.config/i3 ./.config/
cp -r ~/.config/i3status ./.config/
cp -r ~/.config/rofi ./.config/
cp -r ~/wallpapers/ ./
cp ~/.gitconfig ./
sed -i "s/email = .*/\#email = /" .gitconfig
sed -i "s/name = .*/\#name = /" .gitconfig
cp -r ~/.config/termite ./.config/
cp ~/.config/compton.conf ./.config/

# I use aurget and need to store my config somewhere
cp ~/.config/aurgetrc ./.config/

# zsh
cp ~/.zshrc ./.zshrc

